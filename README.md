## URL
https://todolist-540b0.web.app

## DEMO
https://www.loom.com/share/f7f92b4e7c7b4edeb53a89c717886011

# Getting Started with ToDo List App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:
### `npm install`
### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

This application use Firebase to store tasks.
