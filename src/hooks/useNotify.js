import {toast} from "react-toastify";

const useNotify = () =>{

    const success = (msg) => toast.success(msg,{
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });

    const error = (msg) => toast.error(msg,{
        position: "top-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
    });
     return {
         success : success,
         error : error
     }
};

export default useNotify;
