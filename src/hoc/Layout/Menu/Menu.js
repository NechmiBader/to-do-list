import React, {useState} from "react";
import { Navbar, Nav,  Form } from 'react-bootstrap';
import {useTranslation} from "react-i18next";
import "./Menu.css";
import {LinkContainer} from 'react-router-bootstrap';

const Menu = () =>{
    const { t, i18n } = useTranslation();
    const [language,setLanguage] = useState(localStorage.getItem('lang'));
    const handleChangeLanguage = (event) => {
        localStorage.setItem('lang',event.target.value);
        i18n.changeLanguage(event.target.value);
        setLanguage(event.target.value);
    };
    return(<Navbar bg="light" expand="lg">
        <Navbar.Brand href="#home">Todo List</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
           <Nav className="mr-auto">
               <LinkContainer to="/home">
                 <Nav.Link>{t('Home')}</Nav.Link>
               </LinkContainer>
               <LinkContainer to="/tasks">
                    <Nav.Link >{t('Tasks')}</Nav.Link>
               </LinkContainer>
               <LinkContainer to="/logout">
                   <Nav.Link >{t('Logout')}</Nav.Link>
               </LinkContainer>
            </Nav>
            <Form inline>
                <select className="selectpicker" data-width="fit" onChange={handleChangeLanguage} value={language}>
                    <option data-content='<span class="flag-icon flag-icon-us"></span> English' value="en" >English</option>
                    <option data-content='<span class="flag-icon flag-icon-fr"></span> Frensh' value="fr">Français</option>
                </select>
            </Form>
        </Navbar.Collapse>
    </Navbar>)
};

export default Menu;
