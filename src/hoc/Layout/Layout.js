import React from "react";
import Menu from "./Menu/Menu";

const Layout = (props) => {

    return (
        <div>
            <header>
                <Menu/>
            </header>
            <div className="container">
                <div className="col-md-12">
                    {props.children}
                </div>
            </div>
        </div>
    );
};

export default Layout;
