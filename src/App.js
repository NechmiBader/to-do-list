import './App.css';
import './Translation/i18n'
import {Route,Switch,Redirect} from "react-router-dom";
import Home from "./components/Home/Home";
import Logout from "./components/Auth/Logout/Logout";
import React,{Suspense} from "react";
import Login from "./components/Auth/Login/Login";
import Layout from "./hoc/Layout/Layout";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-toastify/dist/ReactToastify.css';
import {ToastContainer} from "react-toastify";
const Tasks = React.lazy(()=>{
    return  import('./components/Tasks/ListTasks/ListTasks');
});
function App() {
    const isAuthenticated = localStorage.getItem('isAuthenticated');

    let routes = (
        <Switch>
            <Route path={['/home', '/tasks','/logout']}>
                <Layout>
                    <Route exact path="/home"><Home /></Route>
                    <Route path="/tasks"><Tasks /></Route>
                    <Route path="/logout"><Logout /></Route>
                </Layout>
            </Route>
            <Route path="/login" render={props=><Login {...props}/>} />
            <Redirect to="/home"/>
        </Switch>
    );

    if(!isAuthenticated){
        routes = (
            <Switch>
                <Route path="/login" render={props=><Login {...props}/>} />
                <Redirect to="/login"/>
            </Switch>
        );
    }
  return (
      <>
          <Suspense fallback={<p>Loading...</p>}>{routes}</Suspense>
          <ToastContainer/>
      </>

  );
}

export default App;
