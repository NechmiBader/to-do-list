import i18n from "i18next";
import {initReactI18next } from "react-i18next";
i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources: {
            fr: {
                translation: {
                    "Tasks": "Tâches",
                    "Logout": "Déconnexion",
                    "Add task": "Ajouter la tâche",
                    "Task name": "Nom de la tâche",
                    "One-line task description": "Description de la tâche en une ligne",
                    "Create new task": "Créer une nouvelle tache",
                    "Task has been created successfully": "Tâche créée avec succès",
                    "Error": "Erreur",
                    "To do-list": "Liste des tâches",
                    "Completed": "Complétée",
                    "Not completed": "Non complétée",
                    "This is home page": "Ceci est la page d'accueil",
                    "Task updated successfully": "Tâche mise à jour avec succès",
                    "Home": "Accueil",
                    "Cancel": "Annuler",
                    "Edit": "Modifier",
                    "Save": "Sauvegarder",
                    "Delete": "Supprimer",
                    "Please verify your credentials and try again": "Veuillez vérifier vos informations d'identification et réessayer",
                    "Task removed successfully": "Tâche supprimée avec succès"
                }
            }
        },
        lng: localStorage.getItem('lang'),
        fallbackLng: localStorage.getItem('lang'),

        interpolation: {
            escapeValue: false
        }
    });

export default i18n;
