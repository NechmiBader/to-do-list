import React from "react";
import {useTranslation} from "react-i18next";
import Typical from 'react-typical'
import "./Home.css";

const Home = () => {
    const {t} = useTranslation();
    const steps = [
        'Hello 👋', 1000,
        t('This is home page')+ ' 👌', 1000
    ];
    return (
        <div className="h-100 d-flex justify-content-center msg">
        <Typical
            steps={steps}
            loop={Infinity}
            wrapper="p"
        />
        </div>
    );
};

export default Home;
