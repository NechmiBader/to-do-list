import React, {useState} from "react";
import {useTranslation} from "react-i18next";
import {db} from "../../../config/firebase";
import useNotify from "../../../hooks/useNotify";
import "./Task.css";
import { PencilSquare, FileExcel } from 'react-bootstrap-icons';
import Tippy from '@tippy.js/react';
import 'tippy.js/dist/tippy.css';

const Task = (props) => {
    const { t } = useTranslation();
    const notify = useNotify();
    const [values,setValues] = useState({
        name : props.name,
        description : props.description
    });
    const [isEditing,setIsEditing] = useState(false);
    const deleteTask = (taskId)=>{
        try {
            db.ref("tasks").child(taskId).remove();
            notify.success(t('Task removed successfully'))
        } catch (error) {
            notify.error(t('Error'));
        }
    };
    const changeStatus = (taskId,status)=>{
      status = status === "Completed" ? "Not completed" : "Completed";
      const newTask = {
          status : status
      };
      updateTask(taskId,newTask);
    };

    const updateTask = (taskId,newTask) => {
        try {
            db.ref(`tasks/${taskId}`).update(newTask);
            notify.success(t('Task updated successfully'));
        } catch (error) {
            notify.error(t('Error'));
        }
    };


    const handleChange = (event) => {
        setValues({
            ...values,
            [event.target.name]: event.target.value
        });
    };

    const handleUpdateTask = () =>{
        if(isEditing){
            updateTask(props.id,values);
        }
        setIsEditing(!isEditing);
    };


    let task = (
        <li className="list-group-item">
            <b>{props.name} : </b>
            {props.description} -
            <span className="link" onClick={()=>deleteTask(props.id)}> {t('Delete')}</span>
            <Tippy content={t('Edit')}>
            <span className="float-right edit-icon"><PencilSquare onClick={()=>{setIsEditing(true)}}/></span>
            </Tippy>
            <span className={`float-right  ${props.status === "Completed" ? 'success-msg' : 'error-msg'}`}
                  onClick={()=>changeStatus(props.id,props.status)}>{t(props.status)} </span>
        </li>
    );

    if(isEditing){
      task = (
          <li className="list-group-item">
              <b><input type="text" value={values.name} name="name" onChange={handleChange}/> </b>
              <input type="text" value={values.description} name="description" onChange={handleChange}/> -
              <span className="link" onClick={()=>deleteTask(props.id)}> {t('Delete')}</span>
              <Tippy content={t('Cancel')}>
                  <span className="float-right edit-icon"><FileExcel onClick={()=>{setIsEditing(false)}}/></span>
              </Tippy>
              <Tippy content={t('Save')}>
                  <span className="float-right edit-icon"><PencilSquare onClick={handleUpdateTask}/></span>
              </Tippy>
              <span className={`float-right  ${props.status === "Completed" ? 'success-msg' : 'error-msg'}`}
                    onClick={()=>changeStatus(props.id,props.status)}>{t(props.status)} </span>
          </li>
      )
    }
    return (<>{task}</>);
};

export default Task;
