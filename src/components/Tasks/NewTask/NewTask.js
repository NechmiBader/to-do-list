import React from "react";
import {useTranslation} from "react-i18next";
import "./NewTask.css";
import { useForm } from "react-hook-form";
import useNotify from "../../../hooks/useNotify";
import { db } from "../../../config/firebase";

const NewTask = () => {

    const { t } = useTranslation();
    const notify = useNotify();
    const { register, handleSubmit, errors } = useForm();
    const onSubmit = (data,e) => {
        const payload = {
            name : data.name,
            description : data.description,
            status : 'Not completed'
        };
        try {
            db.ref("tasks").push(payload);
            notify.success(t('Task has been created successfully'));
            e.target.reset();
        } catch (error) {
            notify.success(t('Error'));
        }
    };


    return (
        <>
            <div className="row">
                <h3>{t('Create new task')}</h3>
            </div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-row">
                    <div className="form-group col-md-4 parent has-error has-feedback" >
                        <label htmlFor="inputEmail4">{t('Task name')}</label>
                        <input type="text"
                               className="form-control child "
                               name="name"
                               aria-invalid={errors.name ? "true" : "false"}
                               ref={register({ required: true })} />
                    </div>
                    <div className="form-group col-md-4 has-error">
                        <label htmlFor="inputPassword4">{t('One-line task description')}</label>
                        <input type="text"
                               className="form-control"
                               name="description"
                               ref={register({ required: true })} />
                    </div>
                    <div className="form-group col-md-3 parent">
                        <input type="submit"
                               disabled={Object.keys(errors).length>0}
                               className="form-control btn btn-primary child"
                               value={t('Add task')}/>
                    </div>
                </div>
            </form>
        </>
    );
};

export default NewTask;
