import React, {useEffect, useState} from "react";
import NewTask from "../NewTask/NewTask";
import {useTranslation} from "react-i18next";
import Spinner from "../../UI/Spinner/Spinner";
import {db} from "../../../config/firebase";
import Task from "../Task/Task";

const ListTasks = () => {
    const { t } = useTranslation();
    const [task,setTasks] = useState([]);
    const [loading,setLoading] = useState(false);

    useEffect(()=>{
        setLoading(true);
        try {
            db.ref("tasks").on("value", snapshot => {
                const fetchedTasks = [];
                snapshot.forEach((snap) => {
                    fetchedTasks.push({
                        ...snap.val(),
                        id : snap.key
                    });
                });
                setTasks(fetchedTasks);
                setLoading(false);
            });
        } catch (error) {
            setLoading(false);
        }
    },[]);
    let listTasks = <Spinner/>;
    if(!loading){
        listTasks = task.map(task=>(
            <Task key={task.id}
                  name={task.name}
                  description={task.description}
                  status={task.status}
                  id={task.id}/>
        ));
    }
    return (
        <div>
        <div className="row">
            <h3>{t('To do-list')}</h3>
        </div>

        <div className="row">
            <ul className="list-group col-md-8">
                {listTasks}
            </ul>
        </div>
        <NewTask/>
        </div>
    );
};

export default ListTasks;
