import React, {useEffect} from "react";
import { withRouter} from 'react-router-dom';
const Logout = ({history}) => {
    useEffect(()=>{
        localStorage.removeItem('isAuthenticated');
        history.push('/login');
    });
    return (<></>);
};

export default withRouter(Logout);
