import React from "react";
import "./Login.css";
import {useForm} from "react-hook-form";
import useNotify from "../../../hooks/useNotify";
import {useTranslation} from "react-i18next";

const Login = ({history}) => {
    const { register, handleSubmit } = useForm({
        defaultValues: {
            username:'test@test.com',
            password: 'test'
        },
    });
    const notify = useNotify();
    const {t} = useTranslation();

    const onSubmit = (data,e) => {
        if(data.username === "test@test.com" && data.password === "test"){
            localStorage.setItem('isAuthenticated',true);
            window.location.href="/";
        }else{
            notify.error(t('Please verify your credentials and try again'))
        }
    };

    return (
        <div className="container login_card h-100">
            <div className="d-flex justify-content-center h-100">
                <div className="user_card">
                    <div className="d-flex justify-content-center form_container">
                        <form onSubmit={handleSubmit(onSubmit)}>
                            <div className="input-group mb-3">
                                <div className="input-group-append">
                                    <span className="input-group-text"><i className="fas fa-user"></i></span>
                                </div>
                                <input type="text" name="username"
                                       ref={register({ required: true })}
                                       className="form-control input_user"
                                       placeholder="username"/>
                            </div>
                            <div className="input-group mb-2">
                                <div className="input-group-append">
                                    <span className="input-group-text"><i className="fas fa-key"></i></span>
                                </div>
                                <input type="password"
                                       name="password"
                                       ref={register({ required: true })}
                                       className="form-control input_pass"
                                       placeholder="password"/>
                            </div>
                            <div className="form-group">

                            </div>
                            <div className="d-flex justify-content-center mt-3 login_container">
                                <button type="submit" name="button" className="btn login_btn">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
