import firebase from 'firebase';
const config = {
    apiKey: "AIzaSyC_RKwkkdRyLBbQnWaqJs32g2VaJzcDVh0",
    authDomain: "todolist-540b0.firebaseapp.com",
    databaseURL: "https://todolist-540b0-default-rtdb.firebaseio.com",
    projectId: "todolist-540b0",
    storageBucket: "todolist-540b0.appspot.com",
    messagingSenderId: "155613843046",
    appId: "1:155613843046:web:3d5d03e72484ad3d3fa7a7"
};
firebase.initializeApp(config);
export const auth = firebase.auth;
export const db = firebase.database();
